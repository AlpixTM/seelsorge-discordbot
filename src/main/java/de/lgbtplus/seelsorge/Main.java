package de.lgbtplus.seelsorge;

import de.lgbtplus.seelsorge.listener.messageBecome;
import de.lgbtplus.seelsorge.utils.DailyClear;
import de.lgbtplus.seelsorge.utils.Resolver;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.TextChannel;

import java.util.Timer;
import java.util.Calendar;

/**
 * Created by xCookiiee on 11.07.2018.
 * Beautiful Names written by Alpix 21.08.2018
 */

public class Main {

  public static Resolver messageResolver;
  private static JDA jda;


  public static void main(String... args) {

    messageResolver = new Resolver();
    messageResolver.loadNames();

    JDABuilder builder = new JDABuilder(AccountType.BOT);
    builder.setToken("*****");
    builder.setAutoReconnect(true);
    builder.setStatus(OnlineStatus.DO_NOT_DISTURB);
    builder.setGame(Game.playing("#Seelsorge"));

    registerEvents(builder);


    try {
      jda = builder.buildBlocking();

    } catch (Exception ex) {
      System.err.println(ex.getMessage());
    }

    Calendar hourSetter = Calendar.getInstance();

    // Name Clear jeden Tag um 4 Uhr
    hourSetter.set(Calendar.HOUR_OF_DAY, 4);
    hourSetter.set(Calendar.MINUTE, 0);
    hourSetter.set(Calendar.SECOND, 0);

    new Timer(true).schedule(new DailyClear(jda), hourSetter.getTime(), 86400000);


    // MSG bei Neustart
    Guild guild = jda.getGuildById(389458795237081089L); // trackt meinen einen Server
    TextChannel channel = guild.getTextChannelById(445656714839195663L); // trackt den Channel auf dem Server
    Main.getResolver().clear();
    channel.sendMessage("**Der Bot wurde neu gestartet, alte BenutzerIDs haben keine Gültigkeit mehr.**").queue();
  }

  public static Resolver getResolver() {
    return messageResolver;
  }


  private static void registerEvents(JDABuilder builder) {
    builder.addEventListener(new messageBecome());
  }


}