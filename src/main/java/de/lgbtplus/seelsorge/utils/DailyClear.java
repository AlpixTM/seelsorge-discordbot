package de.lgbtplus.seelsorge.utils;

import de.lgbtplus.seelsorge.Main;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.TextChannel;

import java.util.TimerTask;

public class DailyClear extends TimerTask {

  private JDA core;
  public DailyClear(JDA jda) {
    core = jda;
  }

  @Override
  public void run() {
    Guild guild = core.getGuildById(389458795237081089L); // trackt meinen einen Server
    TextChannel channel = guild.getTextChannelById(445656714839195663L); // trackt den Channel auf dem Server
    Main.getResolver().clear();
    channel.sendMessage("**Um die Anonymität der Nutzer in der Seelsorge zu wahren, wurde neue BenutzerIDs zufällig vergeben. Diese sind 24h gültig.**").queue();
  }
}
