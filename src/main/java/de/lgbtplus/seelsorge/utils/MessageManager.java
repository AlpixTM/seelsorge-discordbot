package de.lgbtplus.seelsorge.utils;

import de.lgbtplus.seelsorge.Main;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;

import java.awt.*;

/**
 * Created by xCookiiee on 11.07.2018.
 */

public class MessageManager {

  public static Message B(Message msg, String iconURL) {
    String message = "**__Anonyme Nachricht__**\n\n" + msg.getContentRaw();

    MessageBuilder builderOne = new MessageBuilder();
    builderOne.setContent(" ");

    MessageEmbed embedOne = new EmbedBuilder().setColor(Color.CYAN).setDescription(message).setFooter(Main.getResolver().createUID(msg.getAuthor()), iconURL).build();
    builderOne.setEmbed(embedOne);

    return builderOne.build();
  }
}