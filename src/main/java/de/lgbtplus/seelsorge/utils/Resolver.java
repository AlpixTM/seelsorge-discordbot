package de.lgbtplus.seelsorge.utils;

import net.dv8tion.jda.core.entities.User;
import java.util.*;
import java.io.*;
import java.util.concurrent.ThreadLocalRandom;

public class Resolver {


  private Map<String, User> allNames = new HashMap<>(); // Alle Namen der aktuellen Phase (Phase = zwischen daily clear)
  private Map<String, User> names = new HashMap<>();  //curNames
  private Map<Integer, String> namesList = new HashMap<>();
  private Map<String, User> oldNames = new HashMap<>();


  /*
  private List<String> pseudonyme = new ArrayList<>(); {
    pseudonyme.add("");
    pseudonyme.add("");
    pseudonyme.add("");

  }
  */


  public void loadNames() {
    Integer i = 1;
    ClassLoader classLoader = getClass().getClassLoader();
    InputStream inputStream = classLoader.getResourceAsStream("names.txt");
    InputStreamReader isr = new InputStreamReader(inputStream);
    BufferedReader br = new BufferedReader(isr);

    try {
      String line;

      while ((line = br.readLine()) != null)
      {
        String name = line;
        namesList.put(i, name);
        i = new Integer(i.intValue() + 1);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  public String createUID(User user, boolean change) {
    if (names.containsValue(user)) {
      for (String ids : names.keySet()) {
        if (names.get(ids).equals(user)) {
          if( !change) {
            return ids;
          }
          else {
            names.remove(ids);
          }
        }
      }
    }

    // max = Zeilen in der
    int randomNameId = ThreadLocalRandom.current().nextInt( 1, 200);
    randomNameId = new Integer(randomNameId);

    String name = namesList.get(randomNameId);
    //String randomUUID = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 8);
    names.put(name, user);
    allNames.put(name, user);
    return name;
  }

  //Method overloading
  public String createUID(User user) {
    return createUID(user,false);
  }

  public User getUser(String UID) {
    if (allNames.containsKey(UID)) {
      return allNames.get(UID);
    }
    return null;
  }


  public User getOldUser(String UID) {
    if (oldNames.containsKey(UID)) {
      return oldNames.get(UID);
    }
    return null;
  }

  public void clear() {
    oldNames = new HashMap<>(allNames);
    allNames.clear();
    names.clear();
  }
}
